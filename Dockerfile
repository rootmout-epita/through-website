FROM php:7.4-apache

COPY src/apache.conf /etc/apache2/sites-enabled/000-default.conf

COPY src /var/www

WORKDIR /var/www

EXPOSE 80/tcp

CMD ["apache2-foreground"]
